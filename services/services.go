package services

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"user_interest/ui_go_api_gateway/config"
	"user_interest/ui_go_api_gateway/genproto/user_service"
)

type ServiceManagerI interface {
	UserService() user_service.UserServiceClient
}

type grpcClients struct {
	userService user_service.UserServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// User Service...
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		userService: user_service.NewUserServiceClient(connUserService),
	}, nil
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}
